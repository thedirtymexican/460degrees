import boto3
import os
import botocore
import requests

# Public IP used for restricting SSH port
public_ip = requests.get("http://ifconfig.me/ip", headers = {'User-Agent': 'curl 7.1'}).text
cf = boto3.client('cloudformation')

def choose_keypair():
  ec2 = boto3.client('ec2')

  response = ec2.describe_key_pairs()
  keys = [ k['KeyName'] for k in response['KeyPairs'] ]
  
  if len(keys) > 0:
    selection = None
    while selection is None:
      print("Which keypair to use?")
      n = 0
      for k in keys:
        n += 1
        print("{}) {}".format(n, k))
      selection = int(input("Enter # value:"))
      if int(selection) < 1 or int(selection) > n:
        selection = None
    return keys[selection-1]
  else:
    print("Creating keypair webserver...")
    response = ec2.create_key_pair(KeyName="webserver")
    with open("webserver.pem", "w") as pem:
      pem.writelines(response['KeyMaterial'])
    os.chmod("webserver.pem", 0o600)
    print("webserver.pem saved.")
    return "webserver"

def main():
  # Read clodformation template
  template_data = ''
  with open("cf-webserver.yml", "r") as template_file:
    template_data = "".join(template_file.readlines())

  # Parameters to pass to CloudFormtion
  parameters = (
    {
      'ParameterKey': 'KeyName',
      'ParameterValue': keypair
    },
    {
      'ParameterKey': 'InstanceType',
      'ParameterValue': 't2.micro'
    },
    {
      'ParameterKey': 'SSHLocation',
      'ParameterValue': '{}/32'.format(public_ip.strip())
    },
  )

  # StackName
  stack_name = 'WebServer460Degrees'

  stack_result = cf.create_stack(
    StackName=stack_name,
    TemplateBody=template_data, 
    Parameters=parameters
  )
  print("Created stack.")
  print("Waiting for stack to complete.")
  waiter = cf.get_waiter('stack_create_complete')
  waiter.wait(StackName=stack_name)
  print("Stack complete.")
  r = cf.describe_stacks(StackName=stack_name)

  stack, = r['Stacks']
  outputs = stack['Outputs']
  print("\n".join([ "{}: {}".format(o['OutputKey'], o['OutputValue']) for o in outputs ]))



keypair = choose_keypair()

if __name__ == "__main__":
  main()
