# 460degrees webserver deployment test

## Design Rationale
This system is primarily designed to deploy into AWS using the boto3 (AWS Python API) and CloudFormation for orchestration and configuration. It requires a Programatic User to be created with Read and Write ccess to EC2 services.

### Toolset
The scripts works by installing and configuring a Python Virtualenv. Then requirements are installed and it ready to use.

### Scripts
#### requirements.txt
Contains all python requirements that need to be installed to be able to use the AWS Pyhton API and someother supporting tools sets.

#### cf-webserver.yml
This CloudFormation script that contains orchestration and configuration of the web application server through UserData. It creates the following resources:
- Security Groups (with ingress and egress rules)
- EC2 Instance using the lastes Linux AMI

It configures the Web Server by supplying UserData that runs on initialisation. It installs and configures:
- Apache
- mod_wsgi
- pip and virutalenv
- Checkout the 460degrees code
- Starts serving the Flask application through WSGI on port 80

#### provision-webserver.py
The main script is written in python, it connects to AWS through the and API. It's main logic is as follows:
 - Check for keypairs existing. If no keypairs available it then creates one and saves locally with the correct permissions (webserver.pem). 
 - The script then invokes CloudFormation template.

The EC2 instance is attached to a Security Group that only allows port 80 the world and port 22 to the public IP address of the machine running the script.

- SSH KeyPair (if none exist)

## Configuring AWS

Through the AWS console a new user should be created with programatic access (to use AWS CLI), with access the read and write to EC2 services. Once the user is created, an Access Key and Secret Access Key is displayed. Save this information as it is required for the following steps.

### API keys

Supply the keys to the API through evironment variables (much safer than saving into a text file).

```
export AWS_ACCESS_KEY_ID="<supplied from AWS console>"
export AWS_SECRET_ACCESS_KEY_ID="<supplied from AWS console>"
```

## Installation, Configuration and Execution

```
pip install virutalenv
virtualenv venv
. venv/bin/activate
pip install -r requirements
python provision-webserver.py
```
